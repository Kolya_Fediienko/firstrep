import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class GoogleTests {
    private WebDriver driver;
@Test
    public void Search ()
    {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1920, 1080));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://dev2.mycredit.com.ua/ru/about/");
        ArrayList<String> humans = new ArrayList<String>();
        humans.add("/html/body/div[1]/div[4]/div[1]/div/div[1]/div/div"); // о нас
        humans.add("/html/body/div[1]/div[5]/div/div"); //в лицах
        humans.add("/html/body/div[1]/div[4]/div/div[1]/div/p"); // тарифы
        driver.findElement(By.xpath(humans.get(0))).getText();
        String name = driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[1]/div/div[1]/div/div")).getText();
        Assert.assertEquals(name, "О нас");

        driver.quit();
    }

    @Test
    public void Research()
    {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1920, 1080));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://dev2.mycredit.com.ua/ru/news/");
        ArrayList<String> humans = new ArrayList<String>();
        humans.add("/html/body/div[1]/div[4]/div/div[2]/div/div/ul/li[1]/a/figure/img"); // 1
        humans.add("/html/body/div[1]/div[4]/div/div[2]/div/div/ul/li[2]/a/figure/img"); //2
        humans.add("/html/body/div[1]/div[4]/div/div[2]/div/div/ul/li[3]/a/figure/img"); // 3
      //  driver.findElements(By.xpath(humans.get(2))).size()>0;
        String name = driver.findElement(By.xpath("/html/body/div[1]/div[4]/div/div[2]/div/div/ul/li[3]/a/figure/img")).getText();
        Assert.assertEquals(name, "О нас");
    }
}


