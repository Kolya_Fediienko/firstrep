import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import java.sql.Driver;
import java.util.concurrent.TimeUnit;

public class AboutUsPage

{
        protected WebDriver driver;

        @FindBy(xpath = "/html/body/div[1]/div[6]/div/div[1]")//Что для нас важно
        private WebElement ourimportant;

        @FindBy(xpath = "/html/body/div[1]/div[5]/div/div")//MyCredit в лицах
        private WebElement mycreditinface;

        @FindBy(xpath = "/html/body/div[1]/div[4]/div[2]/div[1]")//MyCredit - это:
        private WebElement mycreditis;

        @FindBy(xpath = " /html/body/div[1]/div[4]/div[2]/div[2]/span")//Оформление всего за 20 минут
         private WebElement twentyminutes;

        @FindBy(xpath = "/html/body/div[1]/div[4]/div[1]/figure")//Кнопка плей
        private WebElement play;

        @FindBy(xpath = " //*[@id=\"icon-ico-02\"]/path[1]")//1000 заявок ежедневно:
        private WebElement thousand;

        @FindBy(xpath = " //*[@id=\"js-footer-menu-one\"]/ul[1]/li[1]/a")//MyCredit - это:
        private WebElement badstory;

        @FindBy(xpath = "//*[@id=\"player_uid_324721551_1\"]/div[4]/button")
        private WebElement play2;

        @FindBy(xpath = "/html/body/div[1]/div[4]/div[1]/div/div[1]/div/div")// О нас
        private WebElement element1;

        @FindBy(xpath = "/html/body/div[1]/div[4]/div[2]/div[2]/a")// оформить кредит
        private WebElement makecredit;

        @FindBy(xpath = "/html/body/div[1]/div[3]/div/ul[1]/li[1]/a")// оформить кредит
        private WebElement info;

        @FindBy(xpath = "//*[@id=\"js_navbar\"]/nav/ul[1]/li[6]/a")// оформить кредит
        private WebElement Students;











        @BeforeTest
        protected WebDriver getDriver()
        {
            System.setProperty("webdriver.gecko.driver", "C:\\Firefoxdriver\\geckodriver.exe");
            driver = new FirefoxDriver();
            driver.manage().window().setSize(new Dimension(1920, 1080));
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            PageFactory.initElements(driver,this);
            return driver;
        }

    public void deleteHeader()
    {
        ((JavascriptExecutor) driver).executeScript("var elem = document.getElementById('myAffix');" + "elem.remove();");
    }

    public void click()
    {
        play.click();
    }


    public void open()
    {
        driver.get("http://dev2.mycredit.com.ua/ru/about/");
    }

    public void about()
    {
        String element1 = driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[1]/div/div[1]/div/p")).getText();
        Assert.assertEquals(element1, "Мы стараемся сделать получение займа простым для каждого, при этом придерживаясь нашего основного принципа — помощь должна прийти моментально, как только она потребовалась. Для подписания договора вам не придется ехать к нам в офис. В качестве подписи мы используем цифровой код, которым вы подтверждаете согласие со всеми пунктами договора.");
    }

    public boolean icons ()
    {
        boolean b = driver.findElements(By.xpath("//*[@id=\"icon-ico-01\"]/path[4]")).size() > 0;
        return b;
    }

    public void makecredit()
    {
        Actions action1 = new Actions(driver);
        action1.moveToElement(makecredit).perform();
    }

    public void ourimportant()
    {
        Actions action1 = new Actions(driver);
        action1.moveToElement(ourimportant).perform();
    }


    public void assertreg()
    {
        String REGISTER = driver.findElement(By.xpath("//*[@id=\"js-form-1\"]/div[1]")).getText();
        Assert.assertEquals(REGISTER, "Регистрация в MyCredit" );
    }

    public void URL()
    {
        driver.getCurrentUrl();
        Assert.assertEquals(driver.getCurrentUrl(),"http://dev2.mycredit.com.ua/ru/registration/" );
    }

    public void in_face ()
    {
        String in_face = driver.findElement(By.xpath("/html/body/div[1]/div[5]/div/div")).getText();
        Assert.assertEquals(in_face, "MyCredit в лицах" );
    }

    public boolean in_faceicons ()
    {
        boolean b = driver.findElements(By.xpath("/html/body/div[1]/div[5]/div/ul/li[1]/figure/img")).size() > 0;
        return b;
    }


    public void important ()
    {
        String important = driver.findElement(By.xpath("/html/body/div[1]/div[6]/div/div[1]")).getText();
        Assert.assertEquals(important, "Что для нас важно" );
    }

    public boolean importanticons ()
    {
        boolean importanticons = driver.findElements(By.xpath("//*[@id=\"icon-important-01\"]/path[1]")).size() > 0;
        return importanticons;
    }


    public void info()
    {
        Actions action1 = new Actions(driver);
        action1.moveToElement(info).click().perform();
    }

    public void URLlicense()
    {
        driver.getCurrentUrl();
        Assert.assertEquals(driver.getCurrentUrl(),"http://dev2.mycredit.com.ua/ru/documents-license/" );
    }

    public void Students()
    {
        Students.click();
    }

    public void URLstudent()
    {
        driver.getCurrentUrl();
        Assert.assertEquals(driver.getCurrentUrl(),"http://dev2.mycredit.com.ua/ru/uslugi-kreditovaniya/kredit-dlya-studentov-v-ukraine/" );
    }





}

