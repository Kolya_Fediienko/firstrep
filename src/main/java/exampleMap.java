import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class exampleMap {
    public static void main(String[] args)
    {
        Map<String, String> humans = new HashMap<String, String>();
        humans.put("Игорь", "+380983331113");
        humans.put("Николай", "+380633333333");
        humans.put("Александр", "+312321313213");
        Set<Map.Entry<String, String>> set = humans.entrySet();
        for (Map.Entry<String, String> me : set) {
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());
            String value = humans.get("Николай");
            humans.put("Николай", value + "333");
            System.out.println("У Николая стало " + humans.get("Николай"));
        }
    }
}
